'use strict'

import axios from 'axios';

import * as Constants from '../constants';


export default class FlipkartApiService {
    constructor(logger){
        this.logger = logger;
        this.BASE_URL = "https://affiliate-api.flipkart.net/affiliate",
        this.token = "8ff36996adc1430a9ed39adad2a97e39",
        this.affiliate_id = "joinsunil1"
    }

    getTodaysDOD(){        
        return axios.get(`${this.BASE_URL}${Constants.DOTD_OFFERS}`, {
            headers:{
                'Fk-Affiliate-Id':this.affiliate_id,
                'Fk-Affiliate-Token':this.token,
                'Accept':'application/json'
            }
        })
        .then(({data})=>{
            return Promise.resolve( data );
        })
    }
}