'use strict'

import FlipkartApiService from '../../services/flipkart/FlipkartApiService';

export default class PriceController{
    constructor(logger){
        this.logger = logger;
    }

    getTodaysFlipkartDOTD() {
        const flipkartApiObj = new FlipkartApiService(this.logger);
        
        return flipkartApiObj.getTodaysDOD()
        .then((response)=>{
            console.log(response);
            return Promise.resolve(response);
        })
    }
}