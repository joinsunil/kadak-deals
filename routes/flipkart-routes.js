'use strict'

const express = require('express');
import FlipkartControler from '../controllers/flipkart/DOTDController'

const router = new express.Router();


router.get('/todays-deals',(req, res)=>{
    const flpkrtCtrl = new FlipkartControler(req.log);
    flpkrtCtrl.getTodaysFlipkartDOTD()
    .then((finalResData)=>{
        console.log('7777777777777777777777');
        console.log(finalResData.dotdList);
        res.render('flpkart-dotd', finalResData.dotdList);
    });
});

module.exports = router;