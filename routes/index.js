'use strict'

import {Router} from 'express';
import DOTDCtrl from '../controllers/flipkart/DOTDController';
import flipkartRouter from './flipkart-routes';
import amazonRouter from './amazon-routes';

const router = new Router();

router.get('/',(req, res)=>{
    res.render('home');
});
router.use('/flipkart',flipkartRouter);
router.use('/amazon',amazonRouter);

module.exports=router;