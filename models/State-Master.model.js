module.exports = function(sequelize, DataTypes)
{
    const StateMaster = sequelize.define('StateMaster', {
        state_id:{
            type: DataTypes.INTEGER.UNSIGNED,
            primaryKey: true, 
            autoIncrement:true
        },
        state_name:{
            type:DataTypes.STRING
        },
        active:{
            type:DataTypes.BOOLEAN,
            default:true
        }
    },{
        timestamps: true,
        paranoid: true,
        underscored: true,
        freezeTableName: true,
        tableName: 'state_master',
        version: "1.0",
        classMethods:{
            associate: function(models){
                StateMaster.belongsTo(models.CountryMaster,{foreignKey:'country_id'});
                StateMaster.hasMany(models.CityMaster,{foreignKey:'state_id'});
                StateMaster.hasMany(models.Locations,{foreignKey:'state_id'});
            }
        }
    });

    return StateMaster;
};