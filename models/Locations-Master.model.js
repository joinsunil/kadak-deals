module.exports = function(sequelize, DataTypes)
{
    const LocationMaster = sequelize.define('Locations', {
        location_id: {
            type: DataTypes.INTEGER.UNSIGNED,
            primaryKey: true, 
            autoIncrement:true
        },
        city_name: {
            type:DataTypes.STRING
        },
        active:{
            type:DataTypes.BOOLEAN,
            default:true
        }
    },{
        timestamps: true,
        paranoid: true,
        underscored: true,
        freezeTableName: true,
        tableName: 'location_master',
        version: "1.0",
        classMethods:{
            associate : function(models){
                LocationMaster.belongsTo(models.CountryMaster,{foreignKey:'country_id'});
                LocationMaster.belongsTo(models.StateMaster,{foreignKey:'state_id'});
                LocationMaster.belongsTo(models.CityMaster,{foreignKey:'city_id'});
            }
        }
    });

    return LocationMaster;
};