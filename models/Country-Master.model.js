module.exports = function(sequelize, DataTypes)
{
    const CountryMaster = sequelize.define('CountryMaster', {
        country_id: {
            type: DataTypes.INTEGER.UNSIGNED,
            primaryKey: true, 
            autoIncrement:true
        },
        country_name: {
            type:DataTypes.STRING
        },
        active:{
            type:DataTypes.BOOLEAN,
            default:true
        }
    },{
        timestamps: true,
        paranoid: true,
        underscored: true,
        freezeTableName: true,
        tableName: 'country_master',
        version: "1.0",
        classMethods:{
            associate : function(models){
                CountryMaster.hasMany(models.StateMaster,{foreignKey:'country_id'});
                CountryMaster.hasMany(models.CityMaster,{foreignKey:'country_id'});
                CountryMaster.hasMany(models.Locations,{foreignKey:'country_id'});
            }
        }
    });

    return CountryMaster;
};