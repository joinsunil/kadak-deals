"use strict";

import fs from 'fs';
import path from 'path';
import Sequelize from 'sequelize';

import Logger from '../libs/logger';
const logger = Logger.create();

const env       = process.env.NODE_ENV || "development";
const config    = require(path.join(__dirname, '..', 'config', 'json-config', 'config.json'))[env];

  let sequelize, db = {};

  if (process.env.DATABASE_URL) {
    sequelize = new Sequelize(process.env.DATABASE_URL,config);
  } else {
      sequelize = new Sequelize(config.database, config.username, config.password, {
        logging: function (str) {
          logger.debug( str )
        },
        dialect:'mysql',
        timezone: '+05:30',
        pool: { max: 5, min: 0 }
      });
    }

    fs.readdirSync(__dirname)
    .filter(function(file) {
      return (file.indexOf(".") !== 0) && (file !== "index.js");
    })
    .forEach(function(file) {
      var model = sequelize.import(path.join(__dirname, file));
      db[model.name] = model;
    });
  
  Object.keys(db).forEach(function(modelName) {
    if ("associate" in db[modelName].options['classMethods']) {
      console.log(`Associating ${modelName}`);
      db[modelName].options.classMethods.associate(db);
    }
  });
  
  db.sequelize = sequelize;
  db.Sequelize = Sequelize;

module.exports = db;