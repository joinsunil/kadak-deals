module.exports = function(sequelize, DataTypes)
{
    const CityMaster = sequelize.define('CityMaster', {
        city_id: {
            type: DataTypes.INTEGER.UNSIGNED,
            primaryKey: true, 
            autoIncrement:true
        },
        city_name: {
            type:DataTypes.STRING
        },
        active:{
            type:DataTypes.BOOLEAN,
            default:true
        }
    },{
        timestamps: true,
        paranoid: true,
        underscored: true,
        freezeTableName: true,
        tableName: 'city_master',
        version: "1.0",
        classMethods:{
            associate: function (models){
                CityMaster.hasMany(models.Locations,{foreignKey:'city_id'});
                CityMaster.belongsTo(models.StateMaster,{foreignKey:'state_id'});
            }
        }
    });

    return CityMaster;
};